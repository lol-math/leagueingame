import { useState, useEffect, createContext } from 'react';
import Ddragon from 'ddragon';
import Link from 'next/link';
import { DdragonContext } from '../src/DdragonContext';

// import App from 'next/app'

function MyApp({ Component, pageProps }) {
  const [dd, setDd] = useState<Ddragon>(new Ddragon());
  useEffect(() => {
    (async () => {
      const ddragon = new Ddragon();
      const versions = await fetch(ddragon.versions())
        .then(res => res.json())
        .then(json => json);
      console.log(versions);
      ddragon.version = versions[0];
      setDd(ddragon);
    })();
  }, []);
  return (
    <DdragonContext.Provider value={dd}>
      <div>
        <style jsx>
          {`
            display: grid;
            grid-template-columns: max-content max-content;
            grid-gap: 50px;
          `}
        </style>
        <Link href="/livegame">
          <a>API test</a>
        </Link>
        <Link href="/install">
          <a>Installation</a>
        </Link>
      </div>
      <Component {...pageProps} />
    </DdragonContext.Provider>
  );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp;
