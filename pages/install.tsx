export default () => {
  return (
    <ol>
      <li>
        Install the{' '}
        <a href="/install-cert.cmd" download>
          Riot certificate
        </a>{' '}
        as administrator
      </li>
      <li>Install the extension</li>
    </ol>
  );
};
