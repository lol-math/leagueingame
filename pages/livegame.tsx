import { useEffect, useState } from 'react';
import ChampionStats from '../src/ChampionStats';
import { useContext } from 'react';
import { DdragonContext } from '../src/DdragonContext';

export default () => {
  const [state, setState] = useState(null);
  const ddragon = useContext(DdragonContext);
  const [champions, setChampions] = useState(undefined);

  useEffect(() => {
    const fetchNewGameData = () => {
      fetch('https://localhost:2999/liveclientdata/allgamedata')
        .then(res => res.json())
        .then(json => {
          setState(json);
        });
    };
    const repeat = setInterval(fetchNewGameData, 1000);

    return () => {
      clearInterval(repeat);
    };
  }, []);

  useEffect(() => {
    (async () => {
      const championObject: {
        [championKey: string]: {
          name: string;
        };
      } = await fetch(ddragon.data.champions())
        .then(res => res.json())
        .then(json => json.data);
      const championsByName = {};
      Object.values(championObject).forEach(value => {
        championsByName[value.name] = value;
      });
      setChampions(championsByName);
    })();
  }, [ddragon]);

  if (!state || !champions || !state.allPlayers) return null;
  const playerObject = state.allPlayers.find(
    player => player.summonerName === state.activePlayer.summonerName
  );

  return (
    <>
      <ChampionStats
        stats={state.activePlayer.championStats}
        championKey={champions[playerObject.championName].id}
        level={playerObject.level}
      />
      <div>Gold: {state.activePlayer.currentGold}</div>
      <pre>{JSON.stringify(state, null, 2)}</pre>
    </>
  );
};
