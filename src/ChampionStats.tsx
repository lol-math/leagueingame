import React, { useContext } from 'react';

import Dd from 'ddragon';
import { DdragonContext } from './DdragonContext';
import ResourceBar from './ResourceBar';

const ChampionStatsWrapper = ({ children }) => {
  return <div>{children}</div>;
};

const ChampionTileWithLevel = ({
  level,
  championKey
}: {
  /** Which level to render */
  level: number;
  /** Which champion to render. */
  championKey: string;
}) => {
  const ddragon = useContext(DdragonContext);

  return (
    <>
      <style jsx>
        {`
          .ChampionImageWithLevelWrapper {
            display: inline-block;
            position: relative;
          }
          .LevelPositioner {
            position: absolute;
            width: 100%;
            text-align: center;
            bottom: 0%;
          }
          .LevelWrapper {
            background: #fff;
            border-color: #000;
            border-width: 2px;
            border-style: solid;
            border-radius: 15px;
            padding: 5px 15px;
          }
          .TileImageWrapper {
            overflow: hidden;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
            height: 128px;
          }
        `}
      </style>
      <div className="ChampionImageWithLevelWrapper">
        <div className="LevelPositioner">
          <span className="LevelWrapper">{level}</span>
        </div>
        <div className="TileImageWrapper">
          <img
            src={ddragon.images.tile(championKey, 0)}
            width={128}
            alt={championKey}
          />
        </div>
      </div>
    </>
  );
};

const ChampionStats = ({ championKey, level, stats }): JSX.Element => {
  return (
    <div className="Wrapper">
      <style jsx>
        {`
          .Wrapper {
            margin: 5px 0;
            display: grid;
            grid-template-columns: auto auto auto 1fr;
            max-width: 100%;
            border-radius: 5px;
            background: #fff;
            height: 128px;
          }
          .TextStatsWrapper {
            font-size: 12px;
            padding: 5px;
            box-sizing: border-box;
            display: grid;
            grid-template-columns: repeat(8, 200px);
            grid-gap: 5px;
            height: 128px;
            border: 2px solid #ccc;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            border-left: 0;
          }
        `}
      </style>
      <ChampionTileWithLevel level={level} championKey={championKey} />
      <ResourceBar amount={stats.maxHealth.toFixed(2)} type="Health" />
      <ResourceBar
        amount={stats.resourceMax.toFixed(2)}
        type={stats.resourceType}
      />
      <div className="TextStatsWrapper">
        {Object.keys(stats)
          .filter(statKey => statKey !== 'resourceType')
          .map(statKey => (
            <div>
              <span>{statKey}</span>: <span>{stats[statKey].toFixed(2)}</span>
            </div>
          ))}
      </div>
    </div>
  );
};

export default ChampionStats;
